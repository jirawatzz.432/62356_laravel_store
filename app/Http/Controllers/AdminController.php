<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Report;

class AdminController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles('manager');
        $orders = Order::where('checkout', 1)->orderBy('status', 'asc')->orderBy('created_at', 'desc')->paginate(10);
        // $carts = Order::where('checkout', 1)->orderBy('created_at', 'desc')->first();
        
        return view('admin.order_confirm', compact('orders'));
    }

    public function showOrderdetail(Order $order)
    {   
        return view('admin.showOrderdetail', compact('order'));
    }

    public function updateConfirm(Request $request)
    {
        $order = Order::find($request->id);
        $order->update(['status' => $request->action]);

        $msg = ["id" => $request->id,
            "action" => $request->action];
        return response()->json(array('msg'=> $msg), 200);
    }

    public function indexTopup(Request $request)
    {
        $request->user()->authorizeRoles('manager');
        $carts = Order::where('paid', 1)->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.topup_confirm', compact('carts'));
    }

    public function updateTopup(Request $request)
    {
        $order = Order::find($request->id);
        $user = User::find($order->user_id);
        $oldCredit = $user->credit;
        $action = $request->action > 0 ? 1 : -1; 
        $order->update(["status" => $request->action]);
        $credit = $oldCredit + ($order->products[0]->pivot->total_cost * $action) ;
        $user->update(['credit' => $credit]);

        $msg = ["id" => $request->id,
            "action" => $request->action,
            "credit" => $credit
            
        ];
        return response()->json(array('msg'=> $msg), 200);
    }

    public function reportIndex ()
    {
        $report = Report::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.report', compact('report'));
    }

    public function memberIndex()
    {
        $member = User::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.member', compact('member'));
    }

    public function deleteUser(User $user)
    {
        $order = $user->orders;
        foreach ($order as $key => $value) {
            $value->products()->detach();
        }
        $user->orders()->delete();
        $user->delete();
        return redirect('/admin/memberlist')->with('success', 'Delete user succssfully');
    }
}
