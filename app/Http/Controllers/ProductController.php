<?php

namespace App\Http\Controllers;

use App\Product;
use App\Order;
use App\User;
use App\Cart;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\StoreCheckoutRequest;
use Illuminate\Support\Facades\Storage;
use Session;

class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->query('keyword');
        if ($keyword != '') {
            $products = Product::where('title', 'like', '%'.$keyword.'%')->where('title', '<>', 'topup')->orderBy('created_at', 'desc')->paginate(12);
        }else {
            $products = Product::where('title', '<>', 'topup')->orderBy('created_at', 'desc')->paginate(12);
        }
        if ($request->user())
        {
            $order = Order::firstOrNew(['user_id'=> $request->user()->id, 'status'=> 0]);
            $cart = $order->products->sum('pivot.quantity');
            if($order->checkout !== 1)
            {
                $request->session()->put('cart', $cart);
            }
        }
       
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $product = new Product();

        return view('product.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {   
        if ($request->hasFile('image_file')) {
            $filenameWithExt = $request->file('image_file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image_file')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('image_file')->storeAs('public/images', $fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.png';
        }

        $product = New Product();
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->image_file = $fileNameToStore;

        $product->save();

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize("update", $product);
        if($request->hasFile('image_file')){
            if($product->image_file != 'noimage.png'){
                unlink('storage/images/'.$product->image_file);
            }
            $filenameWithExt = $request->file('image_file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image_file')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('image_file')->storeAs('public/images', $fileNameToStore);
           
        }else {
            $fileNameToStore = $product->image_file;
        }

        $product->update([
            'title' => $request->title,
            'description' => $request->description ?? '',
            'price' => $request->price,
            'image_file' => $fileNameToStore,
        ]);

        return redirect('/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        unlink('storage/images/'.$product->image_file);
        $product->delete();

        return redirect('/product')->with('success', 'Your question has been deleted.');
    }

    public function AddToCart(Request $request)
    {
        $product = Product::where('slug', $request->slug)->first();
        $order = Order::firstOrNew(['user_id'=> $request->user()->id, 'status'=> 0, 'checkout'=>0, 'paid'=>0]);
        $order->save();
        $newQuantity = $request->quantity;
        $oldQuantity = $order->products->find($product->id) !== null ? $order->products->find($product->id)->pivot->quantity: 0;
        if ($request->quantity > $product->quantity) {
            return redirect('/product/'.$request->slug)->with('error', 'Your order have out of stock');
        }
        $quantity = $oldQuantity + $newQuantity;
      
        if ($oldQuantity > 0) {
            $order->products()->updateExistingPivot($product, [
                'quantity' => $quantity,
                'total_cost' => $quantity * $product->price
                ]);
        } else {
            $order->products()->save($product, [
                'quantity' => $quantity,
                'total_cost' => $quantity * $product->price
                ]);
        }

        $product->update(['quantity' => $product->quantity -  $newQuantity]);

        $cart = $order->products->sum('pivot.quantity') + $newQuantity;
        $request->session()->put('cart', $cart);
        $request->session()->put('current', $order->id);
  
        return redirect('/product/'.$request->slug);
    
    }

    public function GetCart(Request $request) {
        $order = Order::firstOrNew(['user_id'=> $request->user()->id, 'status'=> 0, 'checkout'=>0, 'paid'=>0]);
        if($order->checkout === 1){
            return redirect('/checkout');
        }

        $request->session()->put('order', $order);
        $carts = $order->products;

        return view('product.cart', compact('carts'));
    }

    public function updateCart(Request $request) 
    {
        $product = Product::find($request->id);
        $order = Order::firstOrNew(['user_id'=> $request->user()->id, 'status'=> 0, 'checkout'=>0, 'paid'=>0]);
        $oldQuantity = $order->products->find($product)->pivot->quantity;
        $quantity = $oldQuantity + $request->action;
        
        if ($request->action < 0) {
            $product->update(['quantity' => $product->quantity + 1]);
            $order->products()->updateExistingPivot($product, [
                'quantity' => $quantity,
                'total_cost' => $quantity * $product->price
                ]);
            $msg = ["total_cost" => $order->products->sum("pivot.total_cost") + ($product->price*$request->action),
                    "subtotal" => $order->products->find($product)->pivot->total_cost + ($product->price*$request->action),
                    "cart" => $order->products->sum('pivot.quantity') + $request->action
                ];
        }else {
            if ($product->quantity + $request->action < 0) {
                return response()->json(array('msg'=> 'Your order have out of stock'), 200);
            }else {
                $product->update(['quantity' => $product->quantity - 1]);
                $order->products()->updateExistingPivot($product, [
                    'quantity' => $quantity,
                    'total_cost' => $quantity * $product->price
                    ]);
                $msg = ["total_cost" => $order->products->sum("pivot.total_cost") + ($product->price*$request->action),
                    "subtotal" => $order->products->find($product)->pivot->total_cost + ($product->price*$request->action),
                    "cart" => $order->products->sum('pivot.quantity') + $request->action
                ];
            }
        }
        return response()->json(array('msg'=> $msg), 200);
    }

    public function destroyCart(Product $product, Request $request)
    {
        $order = Order::firstOrNew(['user_id'=> $request->user()->id, 'status'=> 0, 'checkout'=>0, 'paid'=>0]);
        $quantity = $order->products()->find($product->id)->pivot->quantity;
        $product->update(['quantity' => $product->quantity + $quantity]);
        $order->products()->detach($product);



        $cart = $order->products->sum('pivot.quantity');
        $request->session()->put('cart', $cart);

        return redirect('/cart');
    }

    public function checkout(StoreCheckoutRequest $request)
    {
        $order = Order::firstOrNew(['user_id'=> $request->user()->id, 'status'=> 0, 'checkout'=>0, 'paid'=>0]);
        $address = $request->name. " " .$request->tel." " .$request->etc ?? ""." ".$request->city. " " .$request->state. " " .$request->postcode;
        $user = User::find($request->user()->id);
        if($user->credit < $order->products->sum('pivot.total_cost')){
            return redirect('/cart')->with('error', "You don't have enough credit");
        }
        $credit = $user->credit - $order->products->sum('pivot.total_cost');
        $user->update(['credit' => $credit]);

        $order->update(['address' => $address,
        'checkout' => 1]);
        $request->session()->forget('cart');
        $request->session()->put('current', $order->id);
 
        return redirect('/checkout')->with('success', "Your question has been submitted");
    }

    public function checkoutIndex(Request $request)
    {
        $current = $request->session()->get('current');
        $order = Order::find($current);
        $checkout = $order->status === 1 ? 'active':'';
        $carts = $order->products;
        $request->session()->forget('cart');
        return view('product.checkout', compact('checkout','carts'));
    }

    public function myPurchase(Request $request)
    {
        $carts = Order::where('user_id', $request->user()->id)->where('checkout', 1)->orderBy('created_at', 'desc')->get();
   
        return view('product.mypurchase', compact('carts'));
    }

    public function topupIndex(Request $request)
    {
        return view('product.topup');
    }

    public function topupStore(Request $request)
    {
        $order = new Order();
        $order->user_id = $request->user()->id;
        $order->paid = 1 ;
        $order->save();

        $product = Product::where('title', 'topup')->first();

        $order->products()->save($product, [
            'quantity' => 1,
            'total_cost' => $request->credit
        ]);

        return redirect('/topup');
    }
}
