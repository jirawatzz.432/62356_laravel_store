<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $user = User::find($request->user()->id);
        return view('profile.index', compact('user'));
    }

    public function edit(User $user)
    {   
        return view('profile.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        if ($request->user()->admin) {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'credit' => $request->credit
            ]);
            return redirect()->route('admin.memberIndex');
        }

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        return redirect()->route('profile.index');
    }
}
