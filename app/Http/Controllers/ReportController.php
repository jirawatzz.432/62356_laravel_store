<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function reportForm ()
    {
        return view('report.report');
    }

    public function reportStore (Request $request)
    {
        $request->user()->reports()->create($request->only('title', 'body'));

        return redirect()->route('report.reportForm')->with('success', 'Your report problem have sent already');
    }
}
