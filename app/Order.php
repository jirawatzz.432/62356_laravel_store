<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $attributes = [
        'status' => 0,
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity', 'total_cost');
    }

    public function getUrlAttribute()
    {
        return route('admin.showOrderdetail', $this->id);
    }

}
