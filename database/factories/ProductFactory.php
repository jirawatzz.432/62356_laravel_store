<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence($nbWords = 1), "."),
        'description' => $faker->paragraphs(rand(3, 5), true),
        'price' => rand(100, 2000)
    ];
});
