@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Report</h2>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Credit (฿)</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($member as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td style="width:50%">{{ $item->email }}</td>
                                <td>{{ number_format($item->credit) }}</td>
                                <td><a href="{{ route('profile.edit', $item->id)}}" class="btn btn-sm btn-outline-info">Edit</a></td>
                                <td>  
                                    @if ($item->roles[0]->id !== 2)
                                        <form class="form-delete" action="{{ route('admin.deleteUser', $item->id)}}" method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure?')">Delete</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">
            {{ $member->links() }}
        </div>
    </div>
@endsection