@extends('layouts.app')
@section('style')
    <style>
        .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 20px;
        }

        /* Hide default HTML checkbox */
        .switch input {
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* The slider */
        .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 12px;
        width: 12px;
        left: 2px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #77c44c;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #77c44c;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(24px);
        -ms-transform: translateX(24px);
        transform: translateX(24px);
        }

        /* Rounded sliders */
        .slider.round {
        border-radius: 34px;
        }

        .slider.round:before {
        border-radius: 50%;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="mb-5">
        <h1>Order</h1>
    </div>
    <div>
        <table class="table cart-table mb-5">
            <thead class="black">
                <tr>
                    <th>Username</th>
                    <th>Address</th>
                    <th>Order date</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($orders as $key => $order)
                    <tr>
                        <td>
                            <span style="color:#000000; font-weight: bold;">
                                {{$order->users->name}}
                            </span>
                        </td>
                        <td>
                            <span style="color:#000000; font-weight: bold;">
                                {{ $order->address }}
                            </span>
                        </td>
                        <td>
                            <span style="color:#000000; font-weight: bold;">
                                {{$order->created_at}}
                            </span>
                        </td>
                        <td>
                            <span style="color:#000000; font-weight: bold;">
                                @if ($order->status ===1)
                                    <span style="color:#000000; font-weight: bold;">
                                        Confirm
                                    </span>
                                @else
                                    <span style="color:#000000; font-weight: bold;">
                                        Pending
                                    </span>
                                @endif
                            </span>
                        </td>
                        <td>
                            <a class="btn btn-outline-success btn-sm" href="{{ $order->url }}" role="button">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            @endforeach
            </tbody>
        </table>
    </div>
        
        <div class="text-center">
            {{ $orders->links() }}
          </div>
    </div>
@endsection