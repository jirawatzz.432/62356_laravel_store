@extends('layouts.app')
@section('style')
    <style>
        .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 20px;
        }

        /* Hide default HTML checkbox */
        .switch input {
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* The slider */
        .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 12px;
        width: 12px;
        left: 2px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #77c44c;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #77c44c;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(24px);
        -ms-transform: translateX(24px);
        transform: translateX(24px);
        }

        /* Rounded sliders */
        .slider.round {
        border-radius: 34px;
        }

        .slider.round:before {
        border-radius: 50%;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="mb-5">
        <h1>Order Detail</h1>
    </div>
    <p><strong>Username: </strong> {{ $order->users->name}}</p>
    <p><strong>Address: </strong> {{ $order->address}}</p>
        <div class="d-flex">
            <div class="mr-auto">
                <strong>Order at: </strong> {{ date('d-F-Y', strtotime($order->created_at)) }}
            </div>
            <label class="switch">
                <input type="checkbox" class="confirm" @if ($order->status === 1) checked @endif>
                <span class="slider round"></span>
            </label>
                @if ($order->status === 1)
                    <span class="text-danger status"> Confirm</span>
                @else
                    <span class="text-danger status"> Pending</span>
                @endif
        </div>
        <table class="table cart-table mb-5">
            <thead class="black">
                <tr>
                    <th colspan="2" class="product-name">Product</th>
                    <th>price</th>
                    <th>Quantity</th>
                    <th width="12%">Subtotal</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($order->products as $item)
                <tr>
                    <td>
                        <img src="/storage/images/{{$item->image_file}}" class="img-fluid" style="max-width: 168px;">
                    </td>
                    <td style="width:50%">
                            <h4 class="text-md-left"> {{$item->title}}</h3>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            ฿ {{ number_format($item->price) }}
                        </span>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            {{$item->pivot->quantity}}
                        </span>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            ฿ {{ number_format($item->pivot->total_cost) }}
                        </span>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr >
                <td colspan="6">
                    <div class="d-flex justify-content-end">
                        <p>
                            Total: &nbsp;     
                            <span style="font-size:1.875rem;">
                                ฿ {{  number_format($order->products->sum('pivot.total_cost')) }}
                            </span>
                        </p>
                    
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".confirm").on("click", function(){
                console.log('test')
                save({{$order->id}});
            });
            
            function save(id){
                let action = $(".confirm").is(':checked') ?  1 : 0;
                $.ajax({
                    url : "{{ route('admin.updateConfirm') }}",
                    type : "POST",
                    data : {
                            id : id,
                            action : action,
                    },
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }, 
                    success : function(data) {
                        let action = data.msg.action > 0 ? "Confirm" : "Pending";
                        $(".status").text(action);
                    },
                })
            }
        });
        
    </script>
@endsection
