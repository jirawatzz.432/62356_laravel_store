@extends('layouts.app')
@section('style')
    <style>
        .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 20px;
        }

        /* Hide default HTML checkbox */
        .switch input {
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* The slider */
        .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 12px;
        width: 12px;
        left: 2px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #77c44c;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #77c44c;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(24px);
        -ms-transform: translateX(24px);
        transform: translateX(24px);
        }

        /* Rounded sliders */
        .slider.round {
        border-radius: 34px;
        }

        .slider.round:before {
        border-radius: 50%;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="mb-5">
        <h1>Top-up Order</h1>
    </div>
        <table class="table cart-table mb-5">
            <thead class="black">
                <tr>
                    <th>Username</th>
                    <th>Order at</th>
                    <th width="50%">price</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
    @foreach ($carts as $cart)
            @foreach ($cart->products as $item)
                <tr>
                    <td style="width:30%">
                        <span class="text-md-left"> {{$cart->users->email}}</span>
                    </td>
                    <td style="width:30%">
                            <span class="text-md-left"> {{ date('d-F-Y', strtotime($cart->created_at)) }}</span>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            ฿ {{ number_format($item->pivot->total_cost) }}
                        </span>
                    </td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" class="confirm{{ $cart->id }}" @if ($cart->status === 1) checked @endif>
                            <span class="slider round"></span>
                        </label>
                        @if ($cart->status === 1)
                            <span class="text-danger status{{ $cart->id }}"> Confirm</span>
                        @else
                            <span class="text-danger status{{ $cart->id }}"> Pending</span>
                        @endif
                    </td>
                </tr>
            @endforeach
    @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $carts->links() }}
          </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            @foreach ($carts as $cart)
            $(".confirm{{$cart->id}}").on("click", function(){
                save({{$cart->id}});
            });
            @endforeach

            function save(id){
                let action = $(".confirm"+id).is(':checked') ?  1 : 0;
                $.ajax({
                    url : "{{ route('admin.updateTopup') }}",
                    type : "POST",
                    data : {
                            id : id,
                            action : action,
                    },
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }, 
                    success : function(data) {
                        let action = data.msg.action > 0 ? "Confirm" : "Pending";
                        $(".status"+data.msg.id).text(action);
                        console.log(data.msg.credit)
                    },
                })
            }
        });
        
    </script>
    
@endsection