@csrf
<div class="form-group">
    <label for="product-title">Product Title</label>
    <input type="text" name="title" value="{{ old('title', $product->title)}}" id="product-title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}">
    @if ($errors->has('title'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('title')}}</strong>
        </div>                            
    @endif
</div>
<div class="form-group">
    <label for="product-description">Product Detail</label>
    <textarea name="description" id="product-description" rows="10" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}">{{ old('description', $product->description)}}</textarea>
</div>
<div class="form-group">
    <label for="price">Price</label>
    <input type="number"  class="form-control" id="price" value="{{ old('price', $product->price)}}" name="price">
    @if ($errors->has('price'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('price')}}</strong>
        </div>                            
    @endif
</div>
<div class="form-group">
    <div class="file-upload">
        <div class="image-upload-wrap">
          <input class="file-upload-input" type='file' name="image_file" value="{{ old('image_file', $product->image_file)}}" onchange="readURL(this);" accept="image/*" />
          <div class="drag-text">
            <h3>Drag and drop a file or select add Image</h3>
          </div>
        </div>
        <div class="file-upload-content">
          <img class="file-upload-image" src="/storage/images/{{$product->image_file}}" alt="your image" />
          <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="btn btn-outline-danger btn-lg">Remove <span class="image-title">Uploaded Image</span></button>
          </div>
        </div>
    </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-outline-primary btn-lg"> {{ $buttonText }} </button>
</div>

