@extends('layouts.app')
@section('content')
<div class="container">
    <div class="col-md-12">
        <h1>Cart</h1>
    </div>
    @include('layouts._message')
    <div class="row">
            @if ($carts->isEmpty())
            <div class="col-md-12">
                <div class="d-flex justify-content-center mt-5">
                    <i class="fa fa-shopping-cart fa-5x" aria-hidden="true"></i>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="mb-auto p-2">
                        <h3>Cart empty</h3>
                    </div>
                </div>
             
            </div>
            @else
                <div class="col-md-8">
                    <table class="table cart-table">
                        <thead class="black">
                            <tr>
                                <th colspan="2" class="product-name">Product</th>
                                <th>Quantity</th>
                                <th width="12%">Subtotal</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($carts as $cart)
                            <tr>
                                <td>
                                    <img src="/storage/images/{{$cart->image_file}}" class="img-fluid" style="max-width: 168px;">
                                </td>
                                <td class="product-name">
                                    <div>
                                        <h4 class="text-md-left"> {{$cart->title}}</h3>
                                        <p>฿ {{ number_format($cart->price) }}</p>
                                    </div>
                                </td>
                    
                                <td>
                                    <div class="row">
                                        <div class="col-md-4 pr-1">
                                            <button type="button" class="btn btn-secondary quantity-left-minus{{$cart->id}}" data-type="minus" data-field="">
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            </button>
                                        </div>  
                                        <div class="col-md-4 pr-0 pl-0">
                                        <input type="text" id="quantity{{$cart->id}}" name="quantity{{$cart->id}}" class="form-control input-number" value="{{$cart->pivot->quantity}}" min="1" max="100">
                                        </div>  
                                        <div class="col-md-4 pl-1">
                                            <button type="button" class="btn btn-secondary quantity-right-plus{{$cart->id}}" data-type="plus" data-field="">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span style="color:#000000; font-weight: bold;" class="subtotal{{$cart->id}}">
                                        ฿ {{ number_format($cart->pivot->total_cost) }}
                                    </span>
                                </td>
                                <td>
                                    <form class="form-delete{{$cart->id}}" action="{{ route('product.destroyCart', $cart->slug)}}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm" data-type="plus" data-field="">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    <table class="table">
                        <tr>
                            <td>Total</td>
                            <td>
                                <span style="font-size:24px; font-weight:bold; color:black" class="total_cost">
                                    ฿ {{ number_format($carts->sum('pivot.total_cost')) }}
                                </span>
                            </td>
                        </tr>
                    </table>
                    <div class="mt-4">
                        <form action="{{ route('product.checkout') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" value="" id="name" class="form-control" placeholder="Name">
                                    <span class="text-danger">{{ $errors->first('name')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="tel" value="" id="tel" class="form-control" placeholder="Tel">
                                <span class="text-danger">{{ $errors->first('tel')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="state" value="" id="state" class="form-control" placeholder="State">
                            <span class="text-danger">{{ $errors->first('state')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="city" value="" id="city" class="form-control" placeholder="City">
                            <span class="text-danger">{{ $errors->first('city')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="postcode" value="" id="postcode" class="form-control" placeholder="Postal code">
                            <span class="text-danger">{{ $errors->first('postcode')}}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="etc" value="" id="etc" class="form-control" placeholder="Building, Street and etc...">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger btn-block">Checkout</button>
                            </div>
                        </form>
                    </div>
            @endif
           
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        @foreach ($carts as $cart)
            var quantitiy{{$cart->id}}=0;
            $('.quantity-right-plus{{$cart->id}}').click(function(e){
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity{{$cart->id}} = parseInt($('#quantity{{$cart->id}}').val());
                    // If is not undefined
                        // Increment
                            $.ajax({
                                url : "{{ route('product.updateCart') }}",
                                type : "POST",
                                data : {
                                        id : {{$cart->id}},
                                        action : 1,
                                },
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }, 
                                success : function(data) {
                                    if (data.msg.total_cost) {
                                        $(".total_cost").text("฿ " + data.msg.total_cost.toLocaleString("en"));
                                        $(".subtotal{{$cart->id}}").text("฿ " + data.msg.subtotal.toLocaleString("en"));
                                        $(".cart-badge").text(data.msg.cart);
                                        $('#quantity{{$cart->id}}').val(quantity{{$cart->id}} + 1);
                                    }
                                    },
                                })
                            });
                $('.quantity-left-minus{{$cart->id}}').click(function(e){
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity{{$cart->id}} = parseInt($('#quantity{{$cart->id}}').val());
                    // If is not undefined
                        // Increment
                        if (quantity{{$cart->id}} >1 ){
                        $.ajax({
                            url : "{{ route('product.updateCart') }}",
                            type : "POST",
                            data : {
                                    id : {{$cart->id}},
                                    action : -1,
                            },
                            headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }, 
                            success : function(data) {
                                if (data.msg.total_cost) {
                                    $(".total_cost").text("฿ " + data.msg.total_cost.toLocaleString("en"));
                                    $(".subtotal{{$cart->id}}").text("฿ " + data.msg.subtotal.toLocaleString("en"));
                                    $(".cart-badge").text(data.msg.cart);
                                    if(quantity{{$cart->id}}>1){
                                        $('#quantity{{$cart->id}}').val(quantity{{$cart->id}} - 1);
                                    }
                                }
                            },
                        })
                    }

                });
        @endforeach
    });
   
</script>
@endsection
