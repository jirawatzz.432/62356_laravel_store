@extends('layouts.app')
@section('style')
<style>

  .progressbar {
      counter-reset: step;
  }
  .progressbar li {
      list-style-type: none;
      width: 25%;
      float: left;
      font-size: 12px;
      position: relative;
      text-align: center;
      text-transform: uppercase;
      color: #7d7d7d;
  }
  .progressbar li:before {
      width: 30px;
      height: 30px;
      content: counter(step);
      counter-increment: step;
      line-height: 30px;
      border: 2px solid #7d7d7d;
      display: block;
      text-align: center;
      margin: 0 auto 10px auto;
      border-radius: 50%;
      background-color: white;
  }
  .progressbar li:after {
      width: 100%;
      height: 2px;
      content: '';
      position: absolute;
      background-color: #7d7d7d;
      top: 15px;
      left: -50%;
      z-index: -1;
  }
  .progressbar li:first-child:after {
      content: none;
  }
  .progressbar li.active {
      color: green;
  }
  .progressbar li.active:before {
      border-color: #55b776;
  }
  .progressbar li.active + li:after {
      background-color: #55b776;
  }
</style>
@endsection
@section('content')
<div class="container">
  <h1>Checkout status</h1>
  <div>
    {{$checkout}}
    <ul class="progressbar d-flex justify-content-center">
      <li class="active">Checkout</li>
      <li class="{{$checkout}}">Confirm</li>
    </ul>
  </div>
  
  <table class="table cart-table">
    <thead class="black">
        <tr>
            <th colspan="2" class="product-name">Product</th>
            <th>Quantity</th>
            <th width="12%">Subtotal</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($carts as $cart)
        <tr>
            <td>
                <img src="/storage/images/{{$cart->image_file}}" class="img-fluid" style="max-width: 168px;">
            </td>
            <td class="product-name">
                <div>
                    <h4 class="text-md-left"> {{$cart->title}}</h3>
                    <p>฿ {{ number_format($cart->price) }}</p>
                </div>
            </td>
   
            <td>
     
              <span style="color:#000000; font-weight: bold;">
                {{$cart->pivot->quantity}}
              </span>
                </div>
            </td>
            <td>
                <span style="color:#000000; font-weight: bold;">
                    ฿ {{ number_format($cart->pivot->total_cost) }}
                </span>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection
@section('js')
<script src="{{ asset('js/dragAndDropfile.js') }}" defer></script>
@endsection