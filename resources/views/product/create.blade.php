@extends('layouts.app')

@section('style')
<link href="{{ asset('/css/dragAndDropfile.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <div class="d-flex align-item-center">
                    <h2>Create Product</h2>
                    <div class="ml-auto">
                    <a href="{{ route('product.index') }}" class="btn btn-outline-secondary"> Back to Product</a>
                    </div>
                  </div>
                  
                </div>
                <div class="card-body">
                <form action="{{ route('product.store')}}" method="post" enctype="multipart/form-data">
                    @include('product._form', ['buttonText' => 'Create'])
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="{{ asset('js/dragAndDropfile.js') }}" defer></script>