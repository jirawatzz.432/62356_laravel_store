@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-12 mb-4">
    <div class="d-flex align-item-center">
      <h2>All Product</h2>
      <form class="form-inline ml-auto">
        <input class="form-control mr-sm-2" name="keyword" value="{{Request::query('keyword')}}" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-secondary my-0" type="submit">Search</button>
      </form>
      @can('create', App\Product::class)
        <div class="ml-auto">
          <a href="{{ route('product.create') }}" class="btn btn-outline-secondary"> Create Product</a>
        </div>
      @endcan
      
    </div>
  </div>
  <div class="row">
  @foreach ($products as $product)
  <div class="col-lg-3 col-md-3 mb-4">
    <div class="card h-100">
        <img src="/storage/images/{{$product->image_file}}" class="card-img-top" height="260.5px">
          <div class="card-body">
              <div class="card-title">
                <h5 class="mt-0"> <a href="{{ $product->url }}"> {{ $product->title}}</a>  </h5>
              </div>
              <p>
              {{ str_limit($product->description, 50) }}
              </p>
              <small class="text-muted">฿ {{ number_format($product->price) }} </small>
          </div>
          @can('update', $product)
            <div class="card-footer">
              <div class="m-auto">
                <a href="{{ route('product.edit', $product->slug)}}" class="btn btn-sm btn-outline-info"> Edit</a>
                <form class="form-delete" action="{{ route('product.destroy', $product->slug)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure?')">Delete</button>
                </form>
              </div>
            </div>
          @endcan
      </div>
    </div>
    @endforeach
  </div>
  <div class="text-center">
    {{ $products->links() }}
  </div>
</div>
@endsection
