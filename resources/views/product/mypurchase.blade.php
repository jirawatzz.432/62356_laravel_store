@extends('layouts.app')
@section('content')
<div class="container">
    <div class="mb-5">
        <h1>My purchase</h1>
    </div>
    @foreach ($carts as $cart)
        <div class="d-flex">
            <div class="mr-auto">
                <strong>Order at: </strong> {{ date('d-F-Y', strtotime($cart->created_at)) }}
            </div>
                @if ($cart->status === 1)
                <span class="text-danger"> Confirm</span>
            @else
                <span class="text-danger"> Pending comfirm</span>
            @endif
        </div>
        <table class="table cart-table mb-5">
            <thead class="black">
                <tr>
                    <th colspan="2" class="product-name">Product</th>
                    <th>price</th>
                    <th>Quantity</th>
                    <th width="12%">Subtotal</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($cart->products as $item)
                <tr>
                    <td>
                        <img src="/storage/images/{{$item->image_file}}" class="img-fluid" style="max-width: 168px;">
                    </td>
                    <td style="width:50%">
                            <h4 class="text-md-left"> {{$item->title}}</h3>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            ฿ {{ number_format($item->price) }}
                        </span>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            {{$item->pivot->quantity}}
                        </span>
                    </td>
                    <td>
                        <span style="color:#000000; font-weight: bold;">
                            ฿ {{ number_format($item->pivot->total_cost) }}
                        </span>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr >
                <td colspan="6">
                    <div class="d-flex justify-content-end">
                        <p>
                            Total: &nbsp;     
                            <span style="font-size:1.875rem;">
                                ฿ {{  number_format($cart->products->sum('pivot.total_cost')) }}
                            </span>
                        </p>
                    
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
        @endforeach

</div>
        
@endsection