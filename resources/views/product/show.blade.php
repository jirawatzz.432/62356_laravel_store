@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('product.index')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $product->title }}</li>
        </ol>
      </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @include('layouts._message')
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="/storage/images/{{$product->image_file}}" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                            <h3>{{ $product->title }}</h3>
                            <hr>
                            <p>
                                <span style="font-size:30px; color:#000000;">฿ {{ number_format($product->price) }} </span>
                            </p>
                            <p>
                                <span class="text-muted"> {{$product->description}}</span>
                            </p>
                            <form class="form-add-to-cart" action="{{ route('product.AddToCart', $product->slug)}}" method="get">
                                    <div class="col-md-12">
                                        <div class="row mt-4">
                                            @csrf
                                            <div class="input-group col-md-5 d-flex mb-3">
                                                <p class="pt-1">Quantity</p>
                                                <span class="input-group-btn mr-2">
                                                    <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                                                <span class="input-group-btn ml-2">
                                                    <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="col-md-4 mt-2">
                                                <span class="text-muted">
                                                    {{$product->quantity}}  piece available
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                                <button type="submit" class="btn btn-yellow btn-lg btn-block">Add to cart</button>
                                        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){

    var quantitiy=0;
       $('.quantity-right-plus').click(function(e){
            
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());
            // If is not undefined
                $('#quantity').val(quantity + 1);
                // Increment
        });

         $('.quantity-left-minus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());
            // If is not undefined
                // Increment
                if(quantity>1){
                $('#quantity').val(quantity - 1);
                }
        });
        
    });
</script>
@endsection
