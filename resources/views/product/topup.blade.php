@extends('layouts.app')

@section('style')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <div class="d-flex align-item-center">
                    <h2>Top-up</h2>
                  </div>
                  
                </div>
                <div class="card-body">
                <form action="{{ route('product.topupStore')}}" method="post">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product-title">Top Up Amount(฿)</label>
                            <input type="text" name="credit" id="product-title" class="form-control" placeholder="1000">
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-orange btn-lg btn-block"> Submit </button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')