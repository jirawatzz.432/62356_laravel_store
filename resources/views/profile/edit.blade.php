@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card col-md-6">
        <div class="card-body">
            <h5 class="card-title">My profile</h5>
            <form action="{{ route('profile.update', $user->id)}}" method="post">
                @csrf
                <div class="form-group row">
                    <label for="profile-name" class="col-sm-2 col-form-label"><strong>Name: </strong></label>
                    <div class="col-sm-8">
                        <input type="text" name="name" value="{{ old('name', $user->name)}}" id="profile-name" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="profile-email" class="col-sm-2 col-form-label"><strong>Email: </strong></label>
                    <div class="col-sm-8">
                        <input type="text" name="email" value="{{ old('email', $user->email)}}" id="profile-email" class="form-control">
                    </div>    
                </div>
                <div class="form-group row">
                    <label for="profile-credit" class="col-sm-2 col-form-label"><strong>Credit: </strong></label>
                    @can('create', App\Product::class)
                        <div class="col-sm-8">
                            <input type="text" name="credit" value="{{ old('credit', $user->credit)}}" id="profile-credit" class="form-control">
                        </div>
                    @else
                        <div class="col-sm-8 mt-2 pb-2">
                            <span>  ฿{{ number_format($user->credit) }}</span>
                        </div>
                    @endcan
                   
                </div> 
                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-outline-info">Edit</button>
                </div>
            </form>
            <hr>
        </div>
    </div>

</div>
@endsection
