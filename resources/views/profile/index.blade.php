@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card col-md-6">
        <div class="card-body">
            <h5 class="card-title">My profile</h5>
            <p class="card-text"><strong>Name:</strong> {{$user->name}}</p>
            <p class="card-text"><strong>Email:</strong> {{$user->email}}</p>
            <p class="card-text"><strong>Credit:</strong> ฿{{ number_format($user->credit) }}</p>
            <hr>
            <a href="{{ route('profile.edit', $user->id)}}" class="btn btn-sm btn-outline-info">Edit</a>
        </div>
    </div>

</div>
@endsection
