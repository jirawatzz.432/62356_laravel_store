@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h2>Report</h2>
        </div>
        <div class="card-body">
            <form action="{{ route('report.reportStore')}}" method="post">
                @csrf
                <div class="col-md-6">
                    @include('layouts._message')
                    <div class="form-group">
                        <label for="report-title">Title</label>
                        <input type="text" name="title" id="report-title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="report-body">Description</label>
                        <textarea type="text" name="body" id="report-body" class="form-control"></textarea>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-danger btn-lg btn-block"> Send report </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection