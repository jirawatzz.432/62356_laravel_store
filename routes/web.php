<?php

Auth::routes();

Route::prefix('admin')->group( function() {
    Route::get('/order', 'AdminController@index')->name('admin.index');
    Route::post('/confirm', 'AdminController@updateConfirm')->name('admin.updateConfirm');
    Route::get('/orderdetail/{order}', 'AdminController@showOrderdetail')->name('admin.showOrderdetail');
    Route::get('/topup', 'AdminController@indexTopup')->name('admin.indexTopup');
    Route::post('/topup', 'AdminController@updateTopup')->name('admin.updateTopup');
    Route::get('/report', 'AdminController@reportIndex')->name('admin.reportIndex');
    Route::get('/memberlist', 'AdminController@memberIndex')->name('admin.memberIndex');
    Route::post('/user/{user}', 'AdminController@deleteUser')->name('admin.deleteUser');

});

Route::resource('product', 'ProductController');
Route::get('product/{slug}', 'ProductController@show')->name('product.show');

Route::get('/add-to-cart/{slug}', 'ProductController@AddToCart')->name('product.AddToCart');
Route::get('/cart', 'ProductController@GetCart')->name('product.GetCart');
Route::post('/updatecart', 'ProductController@updateCart')->name('product.updateCart');
Route::post('/destroycart/{product}', 'ProductController@destroyCart')->name('product.destroyCart');
Route::post('/checkout', 'ProductController@checkout')->name('product.checkout');
Route::get('/checkout', 'ProductController@checkoutIndex')->name('product.checkoutIndex');
Route::get('/mypurchase', 'ProductController@myPurchase')->name('product.myPurchase');
Route::get('/topup', 'ProductController@topupIndex')->name('product.topupIndex');
Route::post('/topup', 'ProductController@topupStore')->name('product.topupStore');


Route::prefix('profile')->group( function() {
    Route::get('', 'ProfileController@index')->name('profile.index');
    Route::get('/{user}', 'ProfileController@edit')->name('profile.edit');
    Route::post('/{user}', 'ProfileController@update')->name('profile.update');
});

Route::get('/report', 'ReportController@reportForm')->name('report.reportForm');
Route::post('/report', 'ReportController@reportStore')->name('report.reportStore');



